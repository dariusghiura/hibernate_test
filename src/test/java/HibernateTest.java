import entity.User;
import org.junit.jupiter.api.*;
import service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class HibernateTest {
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static final int TEST_USER_TABLE_SIZE = 5;
    private static final List<User> TEST_USER_LIST = createTestUserList();

    private static List<User> createTestUserList(){
        User u1 = new User("1", "mail1", "pass1");
        User u2 = new User("2", "mail2", "pass2");
        User u3 = new User("3", "mail3", "pass3");
        User u4 = new User("4", "mail4", "pass4");
        User u5 = new User("5", "mail5", "pass5");
        return List.of(u1, u2, u3, u4, u5);
    }

    @BeforeAll
    public static void createDBConnection(){
        entityManagerFactory = Persistence.createEntityManagerFactory("unit");
    }

    @AfterAll
    public static void closeDBConnection(){
        entityManagerFactory.close();
    }

    @BeforeEach
    public void setUp(){
        entityManager = entityManagerFactory.createEntityManager();
    }

    @AfterEach
    public void tearDown(){
        entityManager.close();
    }

    @Test
    public void testDBConnection(){
        assertTrue(entityManagerFactory.isOpen());
    }
    @Test
    public void getAllUsersTest(){
        List<User> userList = UserService.getAllUsers();
        int size = userList.size();

        assertEquals(size, TEST_USER_TABLE_SIZE);
        assertEquals(userList, TEST_USER_LIST);
    }

    @Test
    public void findUserById(){
        User found = UserService.getUserById("5");
        assertEquals("5", found.getId());
    }

    @Test
    public void findUserByIdNotExisting(){
        User found = UserService.getUserById("6");
        assertNull(found);
    }

    @Test
    public void deleteUser(){
        User toBeDeleted = new User("5", "mail5", "pass5");
        UserService.deleteUser(toBeDeleted);

        User found = UserService.getUserById(toBeDeleted.getId());
        assertNull(found);
    }

    @Test
    public void deleteUserNotExisting(){
        User toBeDeleted = new User("7", "mail7", "pass7");
        assertThrows(NullPointerException.class, () -> UserService.deleteUser(toBeDeleted));
    }


}
