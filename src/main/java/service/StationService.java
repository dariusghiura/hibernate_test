package service;

import entity.Route;
import entity.Station;
import entity.User;
import repository.StationRepo;

import java.util.List;
import java.util.UUID;

public class StationService {

    private static final StationRepo stationRepo = new StationRepo();

    public static List<Station> getAllStations(){
        return stationRepo.getAllStations();
    }

    public static void insertStation(String name, List<Route> routes){
        Station s = new Station(name);
        s.setId(UUID.randomUUID().toString());
        stationRepo.insertStation(s);

        s.setRoutes(routes);
        stationRepo.editStation(s);
    }

    public static void editStation(Station toEditStation){
        stationRepo.editStation(toEditStation);
    }
}
