package service;

import entity.Ticket;
import entity.User;
import repository.UserRepo;

import java.util.List;
import java.util.UUID;

public class UserService {
    private static final UserRepo userRepo = new UserRepo();

    public static List<User> getAllUsers(){
        return userRepo.getAllUsers();
    }

    public static void insertUser(String email, String password, List<Ticket> tickets){
        User u = new User(email, password);
        u.setId(UUID.randomUUID().toString());
        userRepo.insertUser(u);

        u.setTickets(tickets);
        userRepo.editUser(u);
    }

    public static void editUser(User toEditUser){
        userRepo.editUser(toEditUser);
    }

    public static void deleteUser(User toBeDeleted){
        userRepo.deleteUser(toBeDeleted.getId());
    }

    public static User getUserById(String userId){
        return userRepo.findUserById(userId);
    }
}
