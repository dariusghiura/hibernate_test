package service;

import entity.Route;
import entity.Station;
import repository.RouteRepo;

import java.util.List;
import java.util.UUID;

public class RouteService {

    private static final RouteRepo routeRepo = new RouteRepo();

    public static List<Route> getAllRoutes(){
        return routeRepo.getAllRoutes();
    }

    public static void insertRoute(String name, List<Station> stations){
        Route r = new Route(name);
        r.setId(UUID.randomUUID().toString());
        routeRepo.insertRoute(r);

        r.setStations(stations);
        routeRepo.editRoute(r);
    }

    public static void editRoute(Route toEditRoute){
        routeRepo.editRoute(toEditRoute);
    }
}
