package service;

import entity.Ticket;
import entity.User;
import repository.TicketRepo;
import repository.UserRepo;

import java.util.List;
import java.util.UUID;

public class TicketService {

    private static final TicketRepo ticketRepo = new TicketRepo();

    public static List<Ticket> getAllTickets(){
        return ticketRepo.getAllTickets();
    }

    public static void insertTicket(User user){
        Ticket t = new Ticket();
        t.setId(UUID.randomUUID().toString());
        t.setActivated(false);
        ticketRepo.insertTicket(t);

        t.setUser(user);
        ticketRepo.editTicket(t);
    }

    public static void editTicket(Ticket toEditTicket){
        ticketRepo.editTicket(toEditTicket);
    }
}
