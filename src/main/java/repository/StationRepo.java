package repository;

import entity.Station;
import entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class StationRepo {
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("unit");

    public List<Station> getAllStations(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String select = "Select s FROM Station s";
        Query query = entityManager.createQuery(select);

        entityManager.getTransaction().begin();
        List<Station> stations = query.getResultList();

        entityManager.close();
        return stations;
    }

    public void insertStation(Station station) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(station);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void editStation(Station toEditStation) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(toEditStation);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void deleteStation(Station toBeDeleted) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        // remove all associations
        Query q = entityManager.createNativeQuery("DELETE FROM route_station WHERE id_station = ?");
        q.setParameter(1, toBeDeleted.getId());
        q.executeUpdate();
        entityManager.remove(toBeDeleted);
        entityManager.flush();
        entityManager.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
