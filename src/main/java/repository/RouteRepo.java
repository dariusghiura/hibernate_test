package repository;

import entity.Route;
import entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class RouteRepo {
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("unit");

    public List<Route> getAllRoutes(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String select = "Select r FROM Route r";
        Query query = entityManager.createQuery(select);

        entityManager.getTransaction().begin();
        List<Route> routes = query.getResultList();

        entityManager.close();
        return routes;
    }

    public void insertRoute(Route route) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(route);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void editRoute(Route toEditRoute) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(toEditRoute);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void deleteRoute(Route toBeDeleted) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        // remove all associations
        Query q = entityManager.createNativeQuery("DELETE FROM route_station WHERE id_route = ?");
        q.setParameter(1, toBeDeleted.getId());
        q.executeUpdate();
        entityManager.remove(toBeDeleted);
        entityManager.flush();
        entityManager.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
