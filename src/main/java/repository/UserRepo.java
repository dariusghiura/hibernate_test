package repository;

import entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class UserRepo {
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("unit");

    public List<User> getAllUsers(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String select = "Select u FROM User u";
        Query query = entityManager.createQuery(select);

        entityManager.getTransaction().begin();
        List<User> users = query.getResultList();

        entityManager.close();
        return users;
    }

    public void insertUser(User user) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void editUser(User toEditUser) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(toEditUser);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public User findUserById(String id) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        User user =	em.find(User.class, id);
        em.close();
        return user;
    }

    public void deleteUser(String id){
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        User toBeDeleted = entityManager.find(User.class, id);
        entityManager.getTransaction().begin();

        // remove all associations for this user
        Query q = entityManager.createNativeQuery("DELETE FROM Ticket WHERE id_user = ?");
        q.setParameter(1, toBeDeleted.getId());
        q.executeUpdate();

        entityManager.remove(toBeDeleted);
        entityManager.flush();
        entityManager.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
