package repository;

import entity.Ticket;
import entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class TicketRepo {
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("unit");

    public List<Ticket> getAllTickets(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        String select = "Select t FROM Ticket t";
        Query query = entityManager.createQuery(select);

        entityManager.getTransaction().begin();
        List<Ticket> tickets = query.getResultList();

        entityManager.close();
        return tickets;
    }

    public void insertTicket(Ticket ticket) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(ticket);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void editTicket(Ticket toEditTicket) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.merge(toEditTicket);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void deleteTicket(Ticket toBeDeleted){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.remove(toBeDeleted);
        entityManager.flush();
        entityManager.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
