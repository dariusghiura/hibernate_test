package entity;

import javax.persistence.*;

@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    private String id;

    @Column
    private boolean activated;

    @ManyToOne
    @JoinColumn (name = "id_user")
    private User user;

    public void setId(String id) {
        this.id = id;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
