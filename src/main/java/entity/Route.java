package entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Route {
    @Id
    private String id;

    @Column
    private String name;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "route_station" ,
            joinColumns = @JoinColumn(name = "id_route"),
            inverseJoinColumns = @JoinColumn(name = "id_station"))
    private List<Station> stations;

    public Route(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }
}
