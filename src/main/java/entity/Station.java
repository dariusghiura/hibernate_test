package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Station {
    @Id
    private String id;

    @Column
    private String name;

    @ManyToMany(mappedBy = "stations")
    private List<Route> routes;

    public Station(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
